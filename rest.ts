import * as http from 'http'

var options = {
    host: 'localhost',
    port: 8080,
    path: ''
};
http.get(options, function(res) {
    if(res.statusCode !== 200) {
      console.log("Something wrong: " + res.statusCode);
    }
    res.on("data", function(chunk) {
      console.log("Response: " + chunk);
    });
}).on('error', function(e) {
    console.log("Got error: " + e.message);
});