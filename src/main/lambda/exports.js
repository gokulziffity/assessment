var AWS = require('aws-sdk')
var s3 = new AWS.S3()
var parse = require('csv-parser')
var csvWriter = require('csv-write-stream')
var writer = csvWriter()
var options = {
    host: 'test.rebex.net',
    port: '22',
    username: 'demo',
    password: 'password'
} // Its an testing SFTP details, We dont have permission to write/delete
var filePath = 'C:/MyData/pub/example/gkl/sample.csv'
var Client = require(ssh2-sftp-client)

exports.handler = function(event, context, callback) {
    var bucketName = event.Records[0].s3.bucket.name
    var bucketKey = event.Records[0].s3.object.key
    var params = {
        Bucket: bucketName,
        Key: bucketKey
    }
    var parser = parse({delimiter: '|'}, function (err, data) {
        data.forEach(function(line) {
          var eventRow = { "start" : line[0]
                        , "end" : line[1]
                        , "title" : line[2]
                        , "description" : line[4]
                        , "email" : line[5]
                        }
         console.log(JSON.stringify(eventRow))
        })    
    })

    var fileData = s3.getObject(params)
                    .createReadStream()
                    .pipe(parser)

    var sftp = new Client()
    sftp.connect(options).then(() => {
        sftp.mkdir('C:/MyData/pub/example/gkl')
        writer.pipe(sftp.createWriteStream(filePath))
        writer.write(fileData)
        writer.end()
        sftp.end()
    })
    callback(null, 'Success')
 }