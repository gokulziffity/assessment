import * as soap from 'soap'
import * as express from 'express'

var app = express()
var router = express.Router()
var capitalCity = ""
app.use('/', router)
router.get('', function (req, res) {
    const url:string = 'http://webservices.oorsprong.org/websamples.countryinfo/CountryInfoService.wso?WSDL'
    soap.createClient(url, function(error, client) {
        if (error) {
            res.send('Error in creating client')
        }
        let countryCode = "AUS"
        const params:object = {sCountryISOCode: countryCode}
        client.CapitalCity(params, function(err, result) {
            if (err) {
                res.send('Error in finding capital city')
            }
            capitalCity = "The capital city for the country code '" + countryCode + "' is "+ result.CapitalCityResult
            res.send(capitalCity)
        })
    })
})
app.listen(8080)

// Note: To verify this output run the rest.ts file in project root directory